const fs = require('fs');
const request = require('request');
const mkdirp = require('mkdirp');

const restAPI = 'https://www.endomondo.com/rest/v1';

const userId = process.argv.pop();

const stats = {
  total: 0,
  sport: {},
  distance: 0,
  ascent: 0,
  descent: 0,
  calories: 0
}

const auth = fs.readFileSync('.auth');

function doRequest(url, cb) {
  request.get(url, getOptions(), (err, res, body) => {
    if (err) {
      console.err(url, err, body);
      process.exit(1)
    }
    if(res.statusCode > 400) {
      console.err(url, res.statusCode, body);
      process.exit(1)
    }

    console.log(url, res.statusCode);
    cb(body);
  });
}

function getHistory(uid, offset, data, cb) {
  data = data || {};
  const limit = 100;
  const url = `${restAPI}/users/${uid}/workouts/history?limit=${limit}&offset=${offset || 0}&expand=workout`
  let found = 0;

  doRequest(url, body => {
    const result = JSON.parse(body);
    const total = result.paging.total;
    result.data.forEach(workout => {
      if (!data[workout.id]) {
        found ++;
        data[workout.id] = workout;
      }
    });
    found && Object.values(data).length < total
      ? getHistory(uid, offset + result.data.length, data, cb)
      : cb(Object.values(data));
  });
}

function getWorkout(options, cb) {
  const uid = options.uid;
  const wid = options.wid;
  const format = options.format || 'tcx';
  const path = `users/${uid}/workouts/${wid}`;
  const url = `${restAPI}/${path}/export?format=${format.toUpperCase()}`;
  const destination = (options.dest || './') + `${path}/${wid}.${format.toLowerCase()}`;

  fs.existsSync(destination)
    ? cb(true)
    : doRequest(url, body => mkdirp(path, () => {
      fs.writeFileSync(destination, body);
      cb(true);
    }));
}

function addAccumulatedStats(workout) {
  stats.total += 1;
  const sport = "s" + workout.sport;
  stats.sport[sport] = stats.sport[sport] ? stats.sport[sport] + 1 : 1;
  stats.distance += workout.distance ? workout.distance : 0;
  stats.ascent += workout.ascent ? workout.ascent : 0;
  stats.descent += workout.descent ? workout.descent : 0 ;
}

function iterateBasic(index, data) {
  const workout = data[index];
  if (!workout) {
    console.log({stats});
    process.exit(0);
  }
  const wid = workout.id;
  const uid = workout.author.id;
  console.log(`[${index}/${data.length}] ${wid}`);
  addAccumulatedStats(workout);
  getWorkout({ wid, uid, format: 'tcx'}, () => {
    getWorkout({ wid, uid, format: 'gpx'}, () => iterateBasic(index +1, data));
  });
}

function getOptions() {
  if (auth) {
    return {
      headers: {
        "Cookie": auth
      }
    }
  }

  console.error(`
    Missing ".auth" file.
    Login to endomondo in your browser, open dev tools and copy
    the Cookie string form the  request onto the ".auth" file.
  `);
  process.exit(1);
}

if (! userId.match(/^\d+$/)) {
  console.error("Missing userId: export.js <USERID> ");
}

getHistory(userId, 0, {}, (data) => {
  fs.writeFileSync(`./users/${userId}/history.json`, JSON.stringify(data, null, 2));
  iterateBasic(0, data);
});
